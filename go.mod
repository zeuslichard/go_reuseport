module gitee.com/zeuslichard/go_reuseport

go 1.17

require (
	github.com/agiledragon/gomonkey v2.0.2+incompatible
	github.com/stretchr/testify v1.7.0
)
