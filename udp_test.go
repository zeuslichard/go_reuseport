// +build linux darwin dragonfly freebsd netbsd openbsd

// Copyright (C) 2017 Max Riveiro
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.

package reuseport

import (
	"errors"
	"syscall"
	"testing"

	"github.com/agiledragon/gomonkey"
	"github.com/stretchr/testify/assert"
)

func moreCaseNewReusablePortPacketConn(t *testing.T) {
	listenerFour, err := NewReusablePortListener("udp6", ":10081")
	if err != nil {
		t.Error(err)
	}
	defer listenerFour.Close()

	listenerFive, err := NewReusablePortListener("udp4", ":10081")
	if err != nil {
		t.Error(err)
	}
	defer listenerFive.Close()

	listenerSix, err := NewReusablePortListener("udp", ":10081")
	if err != nil {
		t.Error(err)
	}
	defer listenerSix.Close()
}

func TestNewReusablePortPacketConn(t *testing.T) {
	listenerOne, err := NewReusablePortPacketConn("udp4", "localhost:10082")
	if err != nil {
		t.Error(err)
	}
	defer listenerOne.Close()

	listenerTwo, err := NewReusablePortPacketConn("udp", "127.0.0.1:10082")
	if err != nil {
		t.Error(err)
	}
	defer listenerTwo.Close()

	listenerThree, err := NewReusablePortPacketConn("udp6", ":10082")
	if err != nil {
		t.Error(err)
	}
	defer listenerThree.Close()

	moreCaseNewReusablePortPacketConn(t)
}

func TestListenPacket(t *testing.T) {
	type args struct {
		proto string
		addr  string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "case1",
			args: args{
				proto: "udp4",
				addr:  "localhost:10082",
			},
			wantErr: false,
		},
		{
			name: "case2",
			args: args{
				proto: "udp",
				addr:  "localhost:10082",
			},
			wantErr: false,
		},
		{
			name: "case3",
			args: args{
				proto: "udp6",
				addr:  ":10082",
			},
			wantErr: false,
		},
		{
			name: "case4",
			args: args{
				proto: "udp4",
				addr:  ":10081",
			},
			wantErr: false,
		},
		{
			name: "case5",
			args: args{
				proto: "udp6",
				addr:  ":10081",
			},
			wantErr: false,
		},
		{
			name: "case6",
			args: args{
				proto: "udp",
				addr:  ":10081",
			},
			wantErr: false,
		},
		{
			name: "case7",
			args: args{
				proto: "udp6_no_ipv_device",
				addr:  "[::1]:10081",
			},
			wantErr: true,
		},
		{
			name: "case8_not_support_proto",
			args: args{
				proto: "xxx",
				addr:  "[::1]:10081",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotL, err := ListenPacket(tt.args.proto, tt.args.addr)
			if (err != nil) != tt.wantErr {
				t.Errorf("ListenPacket() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotL != nil {
				_ = gotL.Close()
			}
		})
	}
}

func BenchmarkNewReusableUDPPortListener(b *testing.B) {
	for i := 0; i < b.N; i++ {
		listener, err := NewReusablePortPacketConn("udp4", "localhost:10082")

		if err != nil {
			b.Error(err)
		} else {
			listener.Close()
		}
	}
}

// TestNewReusablePortPacketConn2 一些边界覆盖
func TestNewReusablePortPacketConn2(t *testing.T) {
	p0 := gomonkey.ApplyFunc(syscall.Socket, func(domain, typ, proto int) (fd int, err error) {
		return -1, errors.New("new socket fd failed")
	})
	//new socket fd failed
	_, err := NewReusablePortPacketConn("udp4", "localhost:10082")
	assert.NotNil(t, err)
	// set re_useport failed
	p0.Reset()
	//stubs seq
	outputs := []gomonkey.OutputCell{
		{
			Values: gomonkey.Params{errors.New("set opt SO_REUSEADDR failed")},
		},
		{
			Values: gomonkey.Params{nil},
		},
		{
			Values: gomonkey.Params{errors.New("set opt 0xf failed")},
		},
		{
			Values: gomonkey.Params{nil},
		},
		{
			Values: gomonkey.Params{nil},
		},
		{
			Values: gomonkey.Params{errors.New("set opt broadcast failed")},
		},
		{
			Values: gomonkey.Params{nil},
		},
	}
	p1 := gomonkey.ApplyFuncSeq(syscall.SetsockoptInt, outputs)
	// SO_REUSEADDR failed
	_, err = NewReusablePortPacketConn("udp4", "localhost:10082")
	assert.NotNil(t, err)
	// reusePort failed
	_, err = NewReusablePortPacketConn("udp4", "localhost:10082")
	assert.NotNil(t, err)
	// SO_BROADCAST failed
	_, err = NewReusablePortPacketConn("udp4", "localhost:10082")
	assert.NotNil(t, err)
	// bind failed
	p1.Reset()
	p2 := gomonkey.ApplyFunc(syscall.Bind, func(fd int, sa syscall.Sockaddr) (err error) {
		return errors.New("bind failed")
	})
	defer p2.Reset()
	_, err = NewReusablePortPacketConn("udp4", "localhost:10082")
	assert.NotNil(t, err)
}
